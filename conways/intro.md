# Conways Game of Life
Conways Game of Life ist eine Simulation, in der auf einem 2D-Raster einzelne Zellen von Runde zu Runde entweder lebendig oder tod sind.

Die vier Regeln des Spiels sind:

```{tableofcontents}
```
